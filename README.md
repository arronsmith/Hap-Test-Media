# Wat
AVI's containing [Hap](https://github.com/Vidvox/hap) video and PCM audio, in each of the Hap flavours, for test purposes.

All media exported to .mov from Adobe Premiere on OS X, then re-wrapped to AVI
using ffmpeg (`ffmpeg -i in -acodec copy -vcodec copy out.avi`)

# License
BSD 3-Clause. See LICENSE.
